# Terminal master
## Basics: moving & exploring folders

* display hello world: echo "Hello World"
* play with variables: hello="Hello World" && echo $hello
* Navigate to home: cd; cd $HOME; cd ~/
* Create a workdir: mkdir workdir
* go inside it: cd workdir
* create alias to go to workdir: `export WORKDIR='$HOME/workdir' && alias cdw='cd $WORKDIR'`
* show hidden files: ls –a
* (optional) pushd & popd
* go to home

## Chain commands & manage output flux

* Install fortune: brew install fortune
* Read the doc: man fortune
* Diference between && and ;
* Difference between > and >>
* put fortune in file: fortune >> that
* Pipe |
* Cat / grep / sed

## Basics: file management

* create empty file: touch that
* put hello in file: echo $hello > that
* display content of file: cat that; more that; less that; head that; tail that
* add other lines to the file: echo $hello >> that
* overwrite content of file: echo $hello > that
* remove file: rm that
* create folder tmp: mkdir tmp
* create several file inside folder: touch one && touch two && touch three
* Copy file/ copy folder: cp one four; cp -r tmp tmp2
* Zip / Tar this folder and untar/unzip it: tar -czvf ; tar –xzvf
* What is a wildcard * ? Try ls *, ls *.py, ls **/*.py
* remove all files with a letter o into them: rm *o*
* remove folder tmp: rm -rf tmp
* VIM basics: how to exit :wq or :q

## Profile bash/zsh

* Take a look at the .zshrc / .bash_profile
* Define functions
* Define a custom alias
* Source the file to apply
* (optional : change shell : open bash inside zsh, and vice versa. command chsh)

## Remote connection

* create ssh key: ssh-keygen -t rsa / use ssh
* download a file from url: wget https://dslv9ilpbe7p1.cloudfront.net/AikQYCLb72WaP1X8gUyOcw_store_header_image
* Use curl

## Scripting in bash

* Play with loops: while true; do fortune; sleep 2; done
* Put that command in a bash script: while true; do fortune; sleep 2; done
* execute script/stop it
* execute script/kill it from another terminal: ps -ef; kill -9; pgrep; pkill
* (optional) execute in screen/recover and kill

# Git master

## Git intro

* What is Git ?
* Git flow introduction and read that later: Source
* Git tutorials:
	* https://github.com/jlord/git-it-electron
	* https://git-scm.com/docs/gittutorial
	* https://learngitbranching.js.org/

## Git practice

* Configure Git
* Create empty folder, git init
* Create Readme.md, git commit
* Create dev branch, go to it, do several commits
* Go back to master, merge dev
* Go to dev, commit one thing
* Create feature_1 from dev, commit two things
* Create feature_2 from dev, commit two things
* Go to dev, merge feature_1, merge feature_2, resolve conf
* Merge to master
* Create a hotfix on master
* Replicate hotfix on dev with cherry pick
* Try git blame
* Try Git stash
* Git checkout to older commit, create branch from it

## Git IDE

* Extensions atom /
